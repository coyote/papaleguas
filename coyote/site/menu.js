// Configuração do LocalStorage para a pontuação
function setStorage(){
    if (typeof(Storage) !== "undefined"){
        // Caso não exista a variável no LocalStorage
        if (!localStorage.score){
            let scoreArray = [0,[],false];
            // Esse array consiste em:
            // scoreArray = [
            //          current_score, 
            //          all_scores,
            //          submitted
            //        ]

            let all_scores = [];

            // Inicialização dos scores globais
            for (let i=0; i<15; i++){
                all_scores.push([0,"AAA"]);
            }

            scoreArray[1] = all_scores;

            localStorage.score = JSON.stringify(scoreArray);
        }
        // Configuração inicial do score
        if (localStorage.score){
            let scoreArray = JSON.parse(localStorage.score);

            // Inicialização dos scores globais
            for (let i=0; i<15; i++){
                scoreArray[1][i] = ([0,"AAA"]);
            }

            localStorage.score = JSON.stringify(scoreArray);
        }
       
    } else {
        alert("LocalStorage is not working.");
    }
}

setStorage();