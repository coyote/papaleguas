
// USANDO O CANVAS ---------------------------------------------------------------------------------------------

// Função que gera um número aleatório entre dois extremos (ambos inclusos)
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}


// Configuração do LocalStorage para a pontuação
function setStorage(type){
    if (typeof(Storage) !== "undefined"){
        // Caso não exista a variável no LocalStorage
        if (!localStorage.score){
            let scoreArray = [0,[],false];
            // Esse array consiste em:
            // scoreArray = [
            //          current_score, 
            //          all_scores,
            //          submitted
            //        ]

            let all_scores = [];

            // Inicialização dos scores globais
            for (let i=0; i<15; i++){
                all_scores.push([0,"AAA"]);
            }

            scoreArray[1] = all_scores;

            localStorage.score = JSON.stringify(scoreArray);
        }
        // Inicialização do score
        if (localStorage.score && type == "set"){
            // Array com todas as informações da pontuação
            let scoreArray = JSON.parse(localStorage.score);
            scoreArray[0] = 0;
            scoreArray[2] = false;
            localStorage.score = JSON.stringify(scoreArray);
        }
        // Atualização do score
        if (localStorage.score && type == "update"){
            // Array com todas as informações da pontuação
            let scoreArray = JSON.parse(localStorage.score);
            scoreArray[0] = scoreValue;
            localStorage.score = JSON.stringify(scoreArray);
        }
       
    } else {
        alert("LocalStorage is not working.");
    }
}



// Função para desenhar o fundo do mapa
function drawBackground(){
    // Configurações da imagem ---------------------------------------
    if (!show_lines){
        division_amount = 0;
    }

    if (!show_arcs){
        arc_amount = 0;
    }

    // Desenhando todas as linhas
    for (let i = 0; i < division_amount+2; i++){
        // Ângulo da iteração atual
        let current_angle = ((i)*(arc_angle)/(division_amount+1)) + (360 - (final_angle)*180/Math.PI);

        // Configurações da linha
        let current_angle_rad = Math.PI * (current_angle) / 180;
        let destination_x = map_x + map_radius*(Math.cos(current_angle_rad));
        let destination_y = map_y - map_radius*(Math.sin(current_angle_rad));

        ctxB.beginPath();
        ctxB.moveTo(map_x + map_padding*(Math.cos(current_angle_rad)), map_y - map_padding*(Math.sin(current_angle_rad)));
        ctxB.lineTo(destination_x, destination_y);
        ctxB.strokeStyle = line_color;
        ctxB.stroke();
        ctxB.closePath();

        // Texto indicando o grau da linha
        // Margem entre o texto e a linha
        let text_margin = 10;
        let text_translation = (current_angle > 90)? ctxB.measureText(String(Math.ceil(current_angle))).width : 0;
        
        ctxB.beginPath();
        ctxB.font = "12px Arial";
        ctxB.fillStyle = "#000";
        destination_x = map_x + (map_radius + text_margin)*(Math.cos(current_angle_rad)) - text_translation;
        destination_y = map_y - (map_radius + text_margin)*(Math.sin(current_angle_rad));
        ctxB.fillText(`${Math.round(current_angle)}º`, destination_x, destination_y);
        ctxB.closePath();
    }

    // Desenhando todas os arcos
    for (let i = 0; i < arc_amount+2; i++){
        // Raio da iteração atual
        let current_radius = ((i)*(map_radius - map_padding)/(arc_amount+1)) + map_padding;
        let current_real_radius =((i)*(map_real_radius)/(arc_amount+1))/10;
        
        // Desenhando o círculo
        ctxB.beginPath();
        ctxB.arc(map_x, map_y, current_radius, start_angle, final_angle, false);
        ctxB.strokeStyle = line_color;
        ctxB.stroke();
        ctxB.closePath();

        // Texto indicando a distância da linha
        // Configurações da legenda
        let destination_x = map_x + current_radius*(Math.cos(final_angle)) - 5;
        let destination_y = map_y + current_radius*(Math.sin(final_angle)) + 22;
        
        ctxB.beginPath();
        ctxB.font = "12px Arial";
        ctxB.fillStyle = "#000";
        ctxB.fillText(`${Math.round(current_real_radius)}cm`, destination_x, destination_y);
        ctxB.closePath();
    }
}


// Função que carrega os pontos
function renderDots(){
    // Resetar o array de coordenadas cartesianas
    blue_dot_array_xy = [];
    let current_angle = 0;
    let real_distance = 0;
    let virtual_distance = 0;
    let current_angle_rad = 0;
    let origin_x = 0;
    let origin_y = 0;
    let destination_x = 0;
    let destination_y = 0;


    for (let i = 0; i < dot_array.length; i++){
        // Ângulo da iteração atual
        current_angle = dot_array[i][0];

        // Distância da iteração atual
        real_distance = dot_array[i][1];

        // Distância virtual da iteração atual
        virtual_distance = actual_radius*(real_distance/map_real_radius);

        // Configurações da linha
        current_angle_rad = Math.PI * (current_angle) / 180;

        // Ponto de origem
        origin_x = map_x + map_padding*(Math.cos(current_angle_rad));
        origin_y = map_y - map_padding*(Math.sin(current_angle_rad));

        // Ponto de renderização
        destination_x = origin_x + virtual_distance*(Math.cos(current_angle_rad));
        destination_y = origin_y - virtual_distance*(Math.sin(current_angle_rad));

        // Adicionar as coordenadas cartesianas do ponto azul no array
        blue_dot_array_xy.push([destination_x, destination_y]);

        // Desenhando o círculo azul
        ctx.beginPath();
        ctx.fillStyle = "#00f";
        ctx.arc(destination_x, destination_y, point_radius, 0, 360, false);       
        ctx.fill();
        ctx.closePath();
    }

    // Coordenadas cartesianas do ponto vermelho
    let red_x = red_dot_array_xy[0][0];
    let red_y = red_dot_array_xy[0][1];

    // Desenhando o círculo vermelho
    ctx.beginPath();
    ctx.fillStyle = "#f00";
    ctx.arc(red_x, red_y, point_radius, 0, 360, false);
    ctx.fill();
    ctx.closePath();
    
    
}


// Função que gera o ponto aleatório
function randomPoint(type, givenAngle = -1){

    // Configurações do ponto aleatório
    let deg_start_angle = max_angle;
    let deg_final_angle = min_angle;

    let random_angle = getRndInteger(deg_final_angle, deg_start_angle);
    if (givenAngle != -1){
        random_angle = givenAngle;
    }
    let random_distance = getRndInteger(min_accepted_radius, map_real_radius);

    // alert(random_angle);

    // let random_point = [random_angle, random_distance];

    // Distância da iteração atual
    let real_distance = random_distance;

    // Distância virtual da iteração atual
    let virtual_distance = actual_radius*(real_distance/map_real_radius);

    // Configurações da linha
    let current_angle_rad = Math.PI * (random_angle) / 180;

    // Ponto de origem
    let origin_x = map_x + map_padding*(Math.cos(current_angle_rad));
    let origin_y = map_y - map_padding*(Math.sin(current_angle_rad));

    // Ponto de renderização
    let destination_x = origin_x + virtual_distance*(Math.cos(current_angle_rad));
    let destination_y = origin_y - virtual_distance*(Math.sin(current_angle_rad));

    if (type == "red"){
        // Resetar o array de coordenadas cartesianas
        red_dot_array_xy = [];

        // Adicionar as coordenadas cartesianas do ponto vermelho no array
        red_dot_array_xy.push([destination_x, destination_y]);

    }else if (type == "blue"){
        // Adicionar as coordenadas cartesianas do ponto vermelho no array
        blue_dot_array_xy.push([destination_x, destination_y]);
        dot_array.push([random_angle, random_distance]);
    }
    
}


// Função que pega o ponto médio de todos os pontos azuis
function meanPoint(){
    // Ângulo e distância média
    // let mean_angle = 0;
    // let mean_distance = 0;

    // Coordenadas cartesianas médias
    let mean_x = 0;
    let mean_y = 0;

    // Resetar o array de coordenadas cartesianas
    green_dot_array_xy = []; 

    // Iterar ao longo dos pontos azuis
    for (let i = 0; i < dot_array.length; i++){
        // Ângulo da iteração atual
        // let current_angle = dot_array[i][0];

        // Distância da iteração atual
        // let real_distance = dot_array[i][1];

        // Distância virtual da iteração atual
        // mean_distance += actual_radius*(real_distance/map_real_radius);

        // Ângulo em rad
        // mean_angle += Math.PI * (current_angle) / 180;

        mean_x += blue_dot_array_xy[i][0];
        mean_y += blue_dot_array_xy[i][1];

    }

    // mean_angle = mean_angle/dot_array.length;
    // mean_distance = mean_distance/dot_array.length;

    mean_x = mean_x/blue_dot_array_xy.length;
    mean_y = mean_y/blue_dot_array_xy.length;


    // Ponto de origem
    // let origin_x = map_x + map_padding*(Math.cos(mean_angle));
    // let origin_y = map_y - map_padding*(Math.sin(mean_angle));

    // Ponto de renderização
    // let destination_x = origin_x + mean_distance*(Math.cos(mean_angle));
    // let destination_y = origin_y - mean_distance*(Math.sin(mean_angle));

    // Adicionar as coordenadas cartesianas do ponto verde no array
    green_dot_array_xy.push([mean_x, mean_y]);

    // Desenhando o círculo
    ctx.beginPath();
    ctx.arc(mean_x, mean_y, point_radius, 0, 360, false);
    ctx.fillStyle = "#0f0";
    ctx.fill();
    ctx.closePath();
}

// Função que calcula a distância entre o ponto verde e o ponto vermelho
function calculateDistance(){

    // Cálculo da distância
    let green_x = green_dot_array_xy[0][0];
    let green_y = green_dot_array_xy[0][1];
    let red_x = red_dot_array_xy[0][0];
    let red_y = red_dot_array_xy[0][1];

    let distance_red_green = Math.sqrt((green_x - red_x)**2 + (green_y - red_y)**2);

    game_distance = distance_red_green;

    console.log(`Distância entre os pontos vermelho e verde: ${distance_red_green} px`);
    console.log(`Na vida real: ${distance_red_green*(map_real_radius/actual_radius)} mm`);
    console.log(`Distância mínima: ${scoreDistance} mm`);
    if (distance_red_green*(map_real_radius/actual_radius) <= scoreDistance){
        console.log(`VOCÊ CONSEGUIU`);
    }

    // Configurações do desenho da linha entre os pontos
    // ctx.beginPath();
    // ctx.moveTo(green_dot_array_xy[0][0], green_dot_array_xy[0][1]);
    // ctx.lineTo(red_dot_array_xy[0][0], red_dot_array_xy[0][1]);
    // ctx.strokeStyle = "#000";
    // ctx.stroke();
    // ctx.closePath();
}

// Função que calcula a distância entre cada um dos pontos azuis
// e o ponto vermelho
function calculateBlueRedDistance(){
    // Cálculo da distância
    let blue_x = 0;
    let blue_y = 0;
    let red_x = red_dot_array_xy[0][0];
    let red_y = red_dot_array_xy[0][1];

    let distance_red_blue = 0;
    let min_distance = 1000000;

    for (let i=0; i<blue_dot_array_xy.length; i++){
        blue_x = blue_dot_array_xy[i][0];
        blue_y = blue_dot_array_xy[i][1];

        distance_red_blue = Math.sqrt((blue_x - red_x)**2 + (blue_y - red_y)**2);
        if (distance_red_blue < min_distance){
            min_distance = distance_red_blue;
        }
    }

    // console.log(`Distância mínima entre os pontos vermelho e azul: ${min_distance} px`);
    // console.log(`Na vida real: ${min_distance*(map_real_radius/actual_radius)} mm`);
    // console.log(`Distância mínima: ${scoreDistance} mm`);
    // if (min_distance*(map_real_radius/actual_radius) <= scoreDistance){
    //     console.log(`VOCÊ CONSEGUIU`);
    // }

    game_distance = min_distance;
}


// Função que pega a mensagem com as coordenadas e renderiza os pontos
function getCoords(){

    // Variável com o texto com as coordenadas
    let stateCoords = stateObj.textContent;

    // Variável com o ângulo e a distância atual
    let current_angle = stateCoords.substring(0, stateCoords.indexOf(","));
    let current_radius = stateCoords.substring(stateCoords.indexOf(",")+1, stateCoords.length);
    
    // console.log(`Ângulo atual: ${current_angle}`);
    // console.log(`Distância atual: ${current_radius}`);

    // console.log(`Ângulo mínimo: ${min_angle}`);
    // console.log(`Ângulo máximo: ${max_angle}`);
    // console.log(`Raio mínimo: ${min_accepted_radius}`);
    // console.log(`Raio máximo: ${map_real_radius}`);


    // Avalia se a distância recebida é válida
    if (current_angle >= min_angle && current_angle <= max_angle && current_radius >= min_accepted_radius && current_radius <= map_real_radius){
        dot_array.push([Number(current_angle),Number(current_radius)]);
    }
    // dot_array = [
    //     [Number(current_angle),Number(current_radius)]
    // ];

    // CONGIFURAÇÕES DO MOTOR REAL
    old_motor_angle = motor_angle;
    motor_angle = current_angle;

}

// Função que gera novos pontos azuis
function bluePointsGenerator(amount, clear, givenAngle = -1){

    if (clear == true){
        // Reseta o array de pontos azuis
        dot_array = [];
        blue_dot_array_xy = []; 
    }

    for (let i=0; i<amount; i++){
        randomPoint("blue", givenAngle);
    }
}


// Função que sobrescreve os ângulos antigos dos pontos azuis
function overwriteBlueDots(){

    // Variável nova do dot array
    let aux_dot_array = dot_array;
    let delete_count = 0;


    for (let i=0; i<(aux_dot_array.length-2); i++){
        // Analisa se entre o ângulo atual e o anterior existe algum ponto antigo
        // 1) Sentido CCW
        // 2) Sentido CW
        if ((aux_dot_array[i][0] > old_motor_angle && aux_dot_array[i][0] <= motor_angle) || (aux_dot_array[i][0] < old_motor_angle && aux_dot_array[i][0] >= motor_angle)){
            dot_array.splice(i+delete_count, 1);
            delete_count++;
        }
        
    }
}


// Função que administra o funcionamento geral do jogo
function gameManager(generate_red_point = false){

    // Limpeza do console
    // console.clear();

    // Reset do canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // ARMAZENAMENTO DAS COORDENADAS RECEBIDAS
    getCoords();

    // GERAÇÃO DE UM PONTO ALEATÓRIO ----------------------------------------------------
    if (generate_red_point == true){
        randomPoint("red");
    }

    // SIMULA RECEPÇÃO DE COORDENADAS DO SERVIDOR ---------------------------------------
    // bluePointsGenerator(1, false, motor_angle);

    // SOBRESCREVER PONTOS ANTIGOS ------------------------------------------------------
    overwriteBlueDots();
    
    // Remove pontos antigos caso haja mais de 180 pontos
    if (dot_array.length > 180){
        splice(0, 1);
    }

    // RENDERIZAÇÃO DOS PONTOS ----------------------------------------------------------
    renderDots();

    // DETERMINAÇÃO DO PONTO MÉDIO DOS PONTOS REAIS -------------------------------------
    // meanPoint(); 

    // DETERMINAÇÃO DA DISTÂNCIA ENTRE O PONTO VERDE E O PONTO VERMELHO -----------------
    // calculateDistance();

    // DETERMINAÇÃO DA DISTÂNCIA ENTRE CADA PONTO AZUL E O VERMELHO
    calculateBlueRedDistance();

    if (game_distance <= scoreDistance){
        randomPoint("red");
        scoreValue += 1000;
        scoreObj.innerText = scoreValue;
        setStorage("update");
    }
    
    // LÓGICA DE ROTAÇÃO DO MOTOR VIRTUAL
    // old_motor_angle = motor_angle;
    // if (motor_direction == "CCW"){
    //     motor_angle += motor_displacement;
    //     if (motor_angle >= max_angle){
    //         motor_direction = "CW";
    //     }
    // }
    // else if (motor_direction == "CW"){
    //     motor_angle -= motor_displacement;
    //     if (motor_angle <= min_angle){
    //         motor_direction = "CCW";
    //     }
    // }
    // if (motor_angle >= max_angle){
    //     motor_angle = max_angle;
    // }
    // if (motor_angle <= min_angle){
    //     motor_angle = min_angle;
    // }

    // console.log(`Ângulo atual: ${motor_angle}`);
    // console.log(`Ângulo anterior: ${old_motor_angle}`);
    
}


// Função que administra o funcionamento do timer do jogo
function timeManager(){
    timeClock += 1;
    timerObj.textContent = timeClock;

    if (timeClock >= 60){
        // clearInterval(coordReceiverSimulator);
        timeClock = -1;
        document.location.href = "ranking.html";
    }
}


// VARIÁVEIS GLOBAIS --------------------------------------------------------------
// 
// 
// CONFIGURAÇÕES INICIAIS DO CANVAS -----------------------------------------------
// Objeto contendo o canvas do Background
var canvasBackground = document.getElementById("background-canvas"); 
var canvas = document.getElementById("map-canvas"); 

// Objeto contendo o container do canvas
var canvas_container = document.getElementById("canvas-container"); 

// Estilos do container
var element_styles = window.getComputedStyle(canvas_container);

// Dimensões do container
var container_width = element_styles.getPropertyValue("width");
container_width = Number(container_width.slice(0, container_width.length - 2));

var container_height = element_styles.getPropertyValue("height");
container_height = Number(container_height.slice(0, container_height.length - 2));

// Definindo a resolução do canvas
var scale = 1;
canvasBackground.width = container_width * scale;
canvasBackground.height = container_height * scale;
canvas.width = container_width * scale;
canvas.height = container_height * scale;

// Objeto contendo a manipulação do canvas
var ctxB = canvasBackground.getContext("2d");
var ctx = canvas.getContext("2d");

// Cor das linhas
var line_color = "#858585";
ctxB.strokeStyle = line_color;


// CONFIGURAÇÕES DAS COORDENADAS 
// Tag que contém as coordenadas
var stateObj = document.getElementById("state"); 


// CONFIGURAÇÕES DO TIMER
// Tag que contém o timer
var timerObj = document.getElementById("partida-timer"); 
var timeClock = 0;


// CONFIGURAÇÕES DA PONTUAÇÃO
// Tag que contém o timer
var scoreObj = document.getElementById("partida-score"); 
var scoreValue = 0;

// Distância mínima aceitável para marcar pontos
var scoreDistance = 30;


// DISTÂNCIA ENTRE O PONTO MÉDIO E O PONTO VIRTUAL
var game_distance = 0;

 
// CONFIGURAÇÕES DO SIMULADOR DO MOTOR
var motor_angle = 10;
var old_motor_angle = 0;
var motor_direction = "CCW";
var motor_displacement = 1;


// CONFIGURAÇÕES DO SENSOR REAL
// Raio da circunferência (medidas da vida real, em milímetros)
var map_real_radius = 700;
var map_real_padding = 10;
var min_accepted_radius = 50;


// CONFIGURAÇÕES DOS PONTOS REAIS
// Array com as coordenadas polares de cada ponto azul
// [angulo (em graus), raio (em milímetros)]
var dot_array = [
    // [70,70],
    // [60,90],
    // [90,100],
    // [120,50],
    // [150,20],
    // [130,96],
    // [103,130],
    // [87,65],
    // [42,200]
];


// Array com as coordenadas cartesianas de cada ponto
var red_dot_array_xy = [] 
var green_dot_array_xy = [] 
var blue_dot_array_xy = [] 


// Tamanho dos pontos
var point_radius = 3;


// DIMENSÕES DO MAPA
// Centro da base da circunferência
var map_x = container_width/2;
var map_y = 0.75*container_height;

// Raio da circunferência (medidas internas do programa)
var map_radius = 0.6*Math.min(container_width, container_height);
var map_padding = (map_real_padding/map_real_radius)*map_radius;

// Tamanho virtual do raio do mapa
var actual_radius = map_radius - map_padding;


// Ângulos de início e fim do arco da circunferência
// Insira o valor em graus (o maior ângulo deve ser o inicial)
var min_angle = 5;
var max_angle = 175;

var start_angle = max_angle;
var final_angle = min_angle;

start_angle = ((360-start_angle)/180) * Math.PI;
final_angle = ((360-final_angle)/180) * Math.PI;

var arc_angle = (final_angle - start_angle)*180/Math.PI;


// CONFIGURAÇÕES DO MAPA
// Configurações para mostrar ou ocultas linhas e arcos
var show_lines = true;
var show_arcs = true;

// Quantidade de linhas transversais na circunferência
let division_amount = 9;

// Quantidade de arcos na circunferência
let arc_amount = 5;


// var coordReceiverSimulator = setInterval(gameManager, 10);
var timerSimulator = setInterval(timeManager, 1000);


// INICIALIZAÇÃO DA PONTUAÇÃO
setStorage("set");

// CRIAÇÃO DO BACKGROUND ------------------------------------------------------------
drawBackground();

// INICIALIZAÇÃO DO JOGO
gameManager(true);


 
// DEBUG DO TAMANHO DA TELA -------------------------------------------------------

// // Escrevendo texto
// ctx.font = "12px Arial";
// ctx.fillText(`Container width: ${container_width}`, 10, 50);

// // Escrevendo texto
// ctx.font = "12px Arial";
// ctx.fillText(`Container height: ${container_height}`, 10, 100);




 