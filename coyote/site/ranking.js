// Configuração do LocalStorage
function setStorage(){
    if (typeof(Storage) !== "undefined"){
        // Caso não exista a variável no LocalStorage
        if (!localStorage.score){
            let scoreArray = [0,[],false];
            // Esse array consiste em:
            // scoreArray = [
            //          current_score, 
            //          all_scores,
            //          submitted
            //        ]

            let all_scores = [];

            // Inicialização dos scores globais
            for (let i=0; i<15; i++){
                all_scores.push([0,"AAA"]);
            }

            scoreArray[1] = all_scores;

            localStorage.score = JSON.stringify(scoreArray);
        }
        // Configuração inicial do score
        if (localStorage.score){
           let scoreArray = JSON.parse(localStorage.score);
           let current_score = scoreArray[0];
           partida_score_tag.textContent = current_score;
        }
       
    } else {
        alert("LocalStorage is not working.");
    }
}


// Função que faz a ordenação das pontuações
function sortScore(){

    if (localStorage.score){
        // Objeto de score armazenado no Local Storage
        let scoreArray = JSON.parse(localStorage.score);

        // Lista com todas as pontuações
        let all_scores = scoreArray[1];

        // Lista nova com as pontuações ordenadas
        let sorted_scores = [];
        for (let j=0; j<15; j++){
            sorted_scores.push([0,"AAA"]);
        }

        // Ordenação das pontuações
        for (let i=0; i<15; i++){
            if (all_scores[i][0] != 0){
                let pos = -1;
                for (let j=0; j<15; j++){
                    if (all_scores[i][0] <= all_scores[j][0]){
                        pos++;
                    }
                }
                while(sorted_scores[pos][0] != 0){
                    pos--;
                }
                sorted_scores[pos] = all_scores[i];
                score_value_tag_array[pos].textContent = all_scores[i][0];
                score_name_tag_array[pos].textContent = all_scores[i][1];
            }  
        }

        if (scoreArray[2] == true){
            name_input.style.display = "none";
            submit_button.style.display = "none";
            finish_button.style.display = "inline-block";
        }else{
            name_input.style.display = "inline-block";
            submit_button.style.display = "inline-block";
            finish_button.style.display = "none";
        }
        scoreArray[1] = sorted_scores;
        localStorage.score = JSON.stringify(scoreArray);

    }else{
        alert("LocalStorage is not working.");
    }
    
}


// Submeter pontuação e nome
function submitScore(){
    if (localStorage.score){
        if (name_input.value.length == 3){
            // Objeto de score armazenado no Local Storage
            let scoreArray = JSON.parse(localStorage.score);

            // Lista com todas as pontuações
            let current_score = scoreArray[0];

            // Lista com todas as pontuações
            let all_scores = scoreArray[1];

            // Iniciais do jogador
            let player_name = name_input.value;

            // Variável com a menor pontuação do jogo
            let min_score = all_scores[0][0];
            let min_score_id = 0;
            for (let i=0; i<15; i++){
                if (all_scores[i][0] <= min_score){
                    min_score = all_scores[i][0];
                    min_score_id = i;
                }
            }
            if (current_score >= min_score){
                all_scores[min_score_id] = [current_score,player_name];
            }

            scoreArray[1] = all_scores;
            scoreArray[2] = true;

            localStorage.score = JSON.stringify(scoreArray);
            document.location.href = "ranking.html";

        }else{
            alert("Insira apenas 3 letras");
        }
        
    }
}

// VARIÁVEIS GLOBAIS COM AS TAGS DE PONTUAÇÃO
var score_value_tag_array = document.getElementsByClassName("score-value");
var score_name_tag_array = document.getElementsByClassName("score-name");
var partida_score_tag = document.getElementById("partida-score");

// VARIÁVEIS GLOBAIS COM OS INPUTS DE SUBMISSÃO
var name_input = document.getElementById("id_query");
var submit_button = document.getElementById("submit_button");
var finish_button = document.getElementById("finish_button");

setStorage();
sortScore();