#include <stdio.h>
#include <Arduino.h>



// Configurações do Ultrassom -----------------------
// 
//define sound speed in cm/uS
#define SOUND_SPEED 0.034

const int trigPin = 5;  // Pino que envia  o sinal ultrassônico
const int echoPin = 18; // Pino que recebe o sinal ultrassônico

long duration; // Tempo de envio e recepção do sinal
float distanceCm; // Distância medida (em cm)


void ultrassonico_init(){
// Configuração dos pinos do Ultrassônico
    pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
    pinMode(echoPin, INPUT); // Sets the echoPin as an Input


}

float ultraSonic(int ligar){

    if (ligar){

        // Clears the trigPin
        digitalWrite(trigPin, LOW);
        delayMicroseconds(2);

        // Sets the trigPin on HIGH state for 10 micro seconds
        digitalWrite(trigPin, HIGH);
        delayMicroseconds(10);
        digitalWrite(trigPin, LOW);
        
        // Reads the echoPin, returns the sound wave travel time in microseconds
        duration = pulseIn(echoPin, HIGH);
        
        // Calculate the distance
        distanceCm = 5*duration * SOUND_SPEED; // 10/2 = 5

        // Prints the distance in the Serial Monitor
        // Serial.print("Distance (mm): ");
        // Serial.println(distanceCm);
        
        // delay(25);
        return (distanceCm);
    }
}