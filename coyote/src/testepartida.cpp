
// Importando bibliotecas necessárias
#include <stdio.h>
#include <ESPAsyncWebServer.h>
#include <WiFi.h>
#include <AsyncTCP.h>


// Substituir com os dados da rede WiFi
const char* ssid = "Em manuntencao";
const char* password = "desligado";

// String com os dados de Posição
char dot_coords[] = "";

// Leds de teste
bool ledState = 0;
const int ledPin = 2;

// Criar objeto AsyncWebServer na porta 80
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

// Variável que armazena o evento retornado pelo site
String current_event = "";

// String contendo o site da partida
const char index_html[] PROGMEM = R"rawliteral(

<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="styles.css">
        <title>Sonar</title>

        <style>
            :root{
                --container-width: 90vw;
                --container-height: 90vh;
            }

            /* Deixa todos os elementos com dimensionamento correto */
            *{
                box-sizing: border-box;
            }

            /* Retira a margem do documento inteiro */
            body{
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;

                margin: 0px;  
                font-family: Arial, Helvetica, sans-serif; 
                height: 100vh;
                background-color: rgb(235, 235, 235);
                /* background-color: #071d03; */
                /* background-color: #f3ed96; */
            }

            /* Estilo do container do canvas */
            #info-container{
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;

                /* background-color: rgb(196, 196, 196); */
                /* border: 2px solid rgb(179, 247, 139); */
            }

            /* Estilo do container do canvas */
            #canvas-container{
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;

                width: var(--container-width);
                height: var(--container-height);

                /* margin: 20px 0px 0px 0px; */

                /* background-color: rgb(196, 196, 196); */
                /* border: 2px solid rgb(179, 247, 139); */
            }

            /* Estilos do canvas */
            canvas{
                width: 90vw;
                height: 90vh;
                /* border: 1px solid red; */
            }

            #info-container *{
                margin: 0px 0px 10px 0px;
            }

            #stack {
                position:relative;
                width: var(--container-width);
                height: var(--container-height);
            }
            #stack > canvas {
                position:absolute;
                left:0;
                top:0;
            }

            #state-container{
                display: inline-block;
            }

        </style>
    </head>
    
    <body>
        <div id="info-container">
            <h3>Timer: <span id="partida-timer">0</span> s</h3>
            <h4>Pontuação: <span id="partida-score">0</span></h4>
            <p id="state-container">state: <span id="state">90.00,50.00</span></p>
        </div>
        <main id="canvas-container">
            <div id="stack">
                <canvas id="background-canvas">
                    Your browser does not support the HTML canvas tag.
                </canvas>
                <canvas id="map-canvas">
                    Your browser does not support the HTML canvas tag.
                </canvas>
            </div>
            
        </main>
    </body>
    <script>

          
        // USANDO O CANVAS ---------------------------------------------------------------------------------------------

        // Função que gera um número aleatório entre dois extremos (ambos inclusos)
        function getRndInteger(min, max) {
            return Math.floor(Math.random() * (max - min + 1) ) + min;
        }


        // Configuração do LocalStorage para a pontuação
        function setStorage(type){
            if (typeof(Storage) !== "undefined"){
                // Caso não exista a variável no LocalStorage
                if (!localStorage.score){
                    let scoreArray = [0,[],false];

                    let all_scores = [];

                    // Inicialização dos scores globais
                    for (let i=0; i<15; i++){
                        all_scores.push([0,"AAA"]);
                    }

                    scoreArray[1] = all_scores;

                    localStorage.score = JSON.stringify(scoreArray);
                }
                // Inicialização do score
                if (localStorage.score && type == "set"){
                    // Array com todas as informações da pontuação
                    let scoreArray = JSON.parse(localStorage.score);
                    scoreArray[0] = 0;
                    scoreArray[2] = false;
                    localStorage.score = JSON.stringify(scoreArray);
                }
                // Atualização do score
                if (localStorage.score && type == "update"){
                    // Array com todas as informações da pontuação
                    let scoreArray = JSON.parse(localStorage.score);
                    scoreArray[0] = scoreValue;
                    localStorage.score = JSON.stringify(scoreArray);
                }
            
            } else {
                alert("LocalStorage is not working.");
            }
        }



        // Função para desenhar o fundo do mapa
        function drawBackground(){
            // Configurações da imagem ---------------------------------------
            if (!show_lines){
                division_amount = 0;
            }

            if (!show_arcs){
                arc_amount = 0;
            }

            // Desenhando todas as linhas
            for (let i = 0; i < division_amount+2; i++){
                // Ângulo da iteração atual
                let current_angle = ((i)*(arc_angle)/(division_amount+1)) + (360 - (final_angle)*180/Math.PI);

                // Configurações da linha
                let current_angle_rad = Math.PI * (current_angle) / 180;
                let destination_x = map_x + map_radius*(Math.cos(current_angle_rad));
                let destination_y = map_y - map_radius*(Math.sin(current_angle_rad));

                ctxB.beginPath();
                ctxB.moveTo(map_x + map_padding*(Math.cos(current_angle_rad)), map_y - map_padding*(Math.sin(current_angle_rad)));
                ctxB.lineTo(destination_x, destination_y);
                ctxB.strokeStyle = line_color;
                ctxB.stroke();
                ctxB.closePath();

                // Texto indicando o grau da linha
                // Margem entre o texto e a linha
                let text_margin = 10;
                let text_translation = (current_angle > 90)? ctxB.measureText(String(Math.ceil(current_angle))).width : 0;
                
                ctxB.beginPath();
                ctxB.font = "12px Arial";
                ctxB.fillStyle = "#000";
                destination_x = map_x + (map_radius + text_margin)*(Math.cos(current_angle_rad)) - text_translation;
                destination_y = map_y - (map_radius + text_margin)*(Math.sin(current_angle_rad));
                ctxB.fillText(`${Math.round(current_angle)}º`, destination_x, destination_y);
                ctxB.closePath();
            }

            // Desenhando todas os arcos
            for (let i = 0; i < arc_amount+2; i++){
                // Raio da iteração atual
                let current_radius = ((i)*(map_radius - map_padding)/(arc_amount+1)) + map_padding;
                let current_real_radius =((i)*(map_real_radius)/(arc_amount+1))/10;
                
                // Desenhando o círculo
                ctxB.beginPath();
                ctxB.arc(map_x, map_y, current_radius, start_angle, final_angle, false);
                ctxB.strokeStyle = line_color;
                ctxB.stroke();
                ctxB.closePath();

                // Texto indicando a distância da linha
                // Configurações da legenda
                let destination_x = map_x + current_radius*(Math.cos(final_angle)) - 5;
                let destination_y = map_y + current_radius*(Math.sin(final_angle)) + 22;
                
                ctxB.beginPath();
                ctxB.font = "12px Arial";
                ctxB.fillStyle = "#000";
                ctxB.fillText(`${Math.round(current_real_radius)}cm`, destination_x, destination_y);
                ctxB.closePath();
            }
        }


        // Função que carrega os pontos
        function renderDots(){
            // Resetar o array de coordenadas cartesianas
            blue_dot_array_xy = [];
            let current_angle = 0;
            let real_distance = 0;
            let virtual_distance = 0;
            let current_angle_rad = 0;
            let origin_x = 0;
            let origin_y = 0;
            let destination_x = 0;
            let destination_y = 0;


            for (let i = 0; i < dot_array.length; i++){
                // Ângulo da iteração atual
                current_angle = dot_array[i][0];

                // Distância da iteração atual
                real_distance = dot_array[i][1];

                // Distância virtual da iteração atual
                virtual_distance = actual_radius*(real_distance/map_real_radius);

                // Configurações da linha
                current_angle_rad = Math.PI * (current_angle) / 180;

                // Ponto de origem
                origin_x = map_x + map_padding*(Math.cos(current_angle_rad));
                origin_y = map_y - map_padding*(Math.sin(current_angle_rad));

                // Ponto de renderização
                destination_x = origin_x + virtual_distance*(Math.cos(current_angle_rad));
                destination_y = origin_y - virtual_distance*(Math.sin(current_angle_rad));

                // Adicionar as coordenadas cartesianas do ponto azul no array
                blue_dot_array_xy.push([destination_x, destination_y]);

                // Desenhando o círculo azul
                ctx.beginPath();
                ctx.fillStyle = "#00f";
                ctx.arc(destination_x, destination_y, point_radius, 0, 360, false);       
                ctx.fill();
                ctx.closePath();
            }

            // Coordenadas cartesianas do ponto vermelho
            let red_x = red_dot_array_xy[0][0];
            let red_y = red_dot_array_xy[0][1];

            // Desenhando o círculo vermelho
            ctx.beginPath();
            ctx.fillStyle = "#f00";
            ctx.arc(red_x, red_y, point_radius, 0, 360, false);
            ctx.fill();
            ctx.closePath();
            
            
        }


        // Função que gera o ponto aleatório
        function randomPoint(type, givenAngle = -1){

            // Configurações do ponto aleatório
            let deg_start_angle = max_angle;
            let deg_final_angle = min_angle;

            let random_angle = getRndInteger(deg_final_angle, deg_start_angle);
            if (givenAngle != -1){
                random_angle = givenAngle;
            }
            let random_distance = getRndInteger(min_accepted_radius, map_real_radius-100);

            // alert(random_angle);

            // let random_point = [random_angle, random_distance];

            // Distância da iteração atual
            let real_distance = random_distance;

            // Distância virtual da iteração atual
            let virtual_distance = actual_radius*(real_distance/map_real_radius);

            // Configurações da linha
            let current_angle_rad = Math.PI * (random_angle) / 180;

            // Ponto de origem
            let origin_x = map_x + map_padding*(Math.cos(current_angle_rad));
            let origin_y = map_y - map_padding*(Math.sin(current_angle_rad));

            // Ponto de renderização
            let destination_x = origin_x + virtual_distance*(Math.cos(current_angle_rad));
            let destination_y = origin_y - virtual_distance*(Math.sin(current_angle_rad));

            if (type == "red"){
                // Resetar o array de coordenadas cartesianas
                red_dot_array_xy = [];

                // Adicionar as coordenadas cartesianas do ponto vermelho no array
                red_dot_array_xy.push([destination_x, destination_y]);

            }else if (type == "blue"){
                // Adicionar as coordenadas cartesianas do ponto vermelho no array
                blue_dot_array_xy.push([destination_x, destination_y]);
                dot_array.push([random_angle, random_distance]);
            }
            
        }


        // Função que calcula a distância entre cada um dos pontos azuis
        // e o ponto vermelho
        function calculateBlueRedDistance(){
            // Cálculo da distância
            let blue_x = 0;
            let blue_y = 0;
            let red_x = red_dot_array_xy[0][0];
            let red_y = red_dot_array_xy[0][1];

            let distance_red_blue = 0;
            let min_distance = 1000000;

            for (let i=0; i<blue_dot_array_xy.length; i++){
                blue_x = blue_dot_array_xy[i][0];
                blue_y = blue_dot_array_xy[i][1];

                distance_red_blue = Math.sqrt((blue_x - red_x)**2 + (blue_y - red_y)**2);
                if (distance_red_blue < min_distance){
                    min_distance = distance_red_blue;
                }
            }

            game_distance = min_distance;
        }


        // Função que pega a mensagem com as coordenadas e renderiza os pontos
        function getCoords(){

            // Variável com o texto com as coordenadas
            let stateCoords = stateObj.textContent;

            // Variável com o ângulo e a distância atual
            let current_angle = stateCoords.substring(0, stateCoords.indexOf(","));
            let current_radius = stateCoords.substring(stateCoords.indexOf(",")+1, stateCoords.length);


            // Avalia se a distância recebida é válida
            if (current_angle >= min_angle && current_angle <= max_angle && current_radius >= min_accepted_radius && current_radius <= map_real_radius){
                dot_array.push([Number(current_angle),Number(current_radius)]);
            }

            // CONGIFURAÇÕES DO MOTOR REAL
            old_motor_angle = motor_angle;
            motor_angle = current_angle;

        }


        // Função que sobrescreve os ângulos antigos dos pontos azuis
        function overwriteBlueDots(){

            // Variável nova do dot array
            let aux_dot_array = dot_array;
            let delete_count = 0;


            for (let i=0; i<(aux_dot_array.length-2); i++){
                // Analisa se entre o ângulo atual e o anterior existe algum ponto antigo
                // 1) Sentido CCW
                // 2) Sentido CW
                if ((aux_dot_array[i][0] > old_motor_angle && aux_dot_array[i][0] <= motor_angle) || (aux_dot_array[i][0] < old_motor_angle && aux_dot_array[i][0] >= motor_angle)){
                    dot_array.splice(i+delete_count, 1);
                    delete_count++;
                }
                
            }
        }


        // Função que administra o funcionamento geral do jogo
        function gameManager(generate_red_point = false){

            // Limpeza do console
            // console.clear();

            // Reset do canvas
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            // ARMAZENAMENTO DAS COORDENADAS RECEBIDAS
            getCoords();

            // GERAÇÃO DE UM PONTO ALEATÓRIO ----------------------------------------------------
            if (generate_red_point == true){
                randomPoint("red");
            }

            // SIMULA RECEPÇÃO DE COORDENADAS DO SERVIDOR ---------------------------------------
            // bluePointsGenerator(1, false, motor_angle);

            // SOBRESCREVER PONTOS ANTIGOS ------------------------------------------------------
            overwriteBlueDots();
            
            // Remove pontos antigos caso haja mais de 180 pontos
            if (dot_array.length > 180){
                splice(0, 1);
            }

            // RENDERIZAÇÃO DOS PONTOS ----------------------------------------------------------
            renderDots();

            // DETERMINAÇÃO DA DISTÂNCIA ENTRE CADA PONTO AZUL E O VERMELHO
            calculateBlueRedDistance();

            if (game_distance <= scoreDistance){
                randomPoint("red");
                scoreValue += 1000;
                scoreObj.innerText = scoreValue;
                setStorage("update");
            }

            
        }


        // Função que administra o funcionamento do timer do jogo
        function timeManager(){
            timeClock += 1;
            timerObj.textContent = timeClock;

            if (timeClock >= 60){
                timeClock = -1;
                gameOver();
                document.location.href = "/ranking";
            }
        }


        // VARIÁVEIS GLOBAIS --------------------------------------------------------------
        // 
        // 
        // CONFIGURAÇÕES INICIAIS DO CANVAS -----------------------------------------------
        // Objeto contendo o canvas do Background
        var canvasBackground = document.getElementById("background-canvas"); 
        var canvas = document.getElementById("map-canvas"); 

        // Objeto contendo o container do canvas
        var canvas_container = document.getElementById("canvas-container"); 

        // Estilos do container
        var element_styles = window.getComputedStyle(canvas_container);

        // Dimensões do container
        var container_width = element_styles.getPropertyValue("width");
        container_width = Number(container_width.slice(0, container_width.length - 2));

        var container_height = element_styles.getPropertyValue("height");
        container_height = Number(container_height.slice(0, container_height.length - 2));

        // Definindo a resolução do canvas
        var scale = 1;
        canvasBackground.width = container_width * scale;
        canvasBackground.height = container_height * scale;
        canvas.width = container_width * scale;
        canvas.height = container_height * scale;

        // Objeto contendo a manipulação do canvas
        var ctxB = canvasBackground.getContext("2d");
        var ctx = canvas.getContext("2d");

        // Cor das linhas
        var line_color = "#858585";
        ctxB.strokeStyle = line_color;


        // CONFIGURAÇÕES DAS COORDENADAS 
        // Tag que contém as coordenadas
        var stateObj = document.getElementById("state"); 


        // CONFIGURAÇÕES DO TIMER
        // Tag que contém o timer
        var timerObj = document.getElementById("partida-timer"); 
        var timeClock = 0;


        // CONFIGURAÇÕES DA PONTUAÇÃO
        // Tag que contém o timer
        var scoreObj = document.getElementById("partida-score"); 
        var scoreValue = 0;

        // Distância mínima aceitável para marcar pontos
        var scoreDistance = 100;


        // DISTÂNCIA ENTRE O PONTO MÉDIO E O PONTO VIRTUAL
        var game_distance = 0;

        
        // CONFIGURAÇÕES DO SIMULADOR DO MOTOR
        var motor_angle = 10;
        var old_motor_angle = 0;
        var motor_direction = "CCW";
        var motor_displacement = 1;


        // CONFIGURAÇÕES DO SENSOR REAL
        // Raio da circunferência (medidas da vida real, em milímetros)
        var map_real_radius = 700;
        var map_real_padding = 10;
        var min_accepted_radius = 50;


        // CONFIGURAÇÕES DOS PONTOS REAIS
        // Array com as coordenadas polares de cada ponto azul
        // [angulo (em graus), raio (em milímetros)]
        var dot_array = [
            // [70,70],
            // [60,90],
            // [90,100],
            // [120,50],
            // [150,20],
            // [130,96],
            // [103,130],
            // [87,65],
            // [42,200]
        ];


        // Array com as coordenadas cartesianas de cada ponto
        var red_dot_array_xy = [] 
        var green_dot_array_xy = [] 
        var blue_dot_array_xy = [] 


        // Tamanho dos pontos
        var point_radius = 7;


        // DIMENSÕES DO MAPA
        // Centro da base da circunferência
        var map_x = container_width/2;
        var map_y = 0.75*container_height;

        // Raio da circunferência (medidas internas do programa)
        var map_radius = 0.6*Math.min(container_width, container_height);
        var map_padding = (map_real_padding/map_real_radius)*map_radius;

        // Tamanho virtual do raio do mapa
        var actual_radius = map_radius - map_padding;


        // Ângulos de início e fim do arco da circunferência
        // Insira o valor em graus (o maior ângulo deve ser o inicial)
        var min_angle = 20;
        var max_angle = 160;

        var start_angle = max_angle;
        var final_angle = min_angle;

        start_angle = ((360-start_angle)/180) * Math.PI;
        final_angle = ((360-final_angle)/180) * Math.PI;

        var arc_angle = (final_angle - start_angle)*180/Math.PI;


        // CONFIGURAÇÕES DO MAPA
        // Configurações para mostrar ou ocultas linhas e arcos
        var show_lines = true;
        var show_arcs = true;

        // Quantidade de linhas transversais na circunferência
        let division_amount = 9;

        // Quantidade de arcos na circunferência
        let arc_amount = 5;


        // var coordReceiverSimulator = setInterval(gameManager, 10);
        var timerSimulator = setInterval(timeManager, 1000);


        // INICIALIZAÇÃO DA PONTUAÇÃO
        setStorage("set");

        // CRIAÇÃO DO BACKGROUND ------------------------------------------------------------
        drawBackground();

        // INICIALIZAÇÃO DO JOGO
        gameManager(true);
        


 

        // CONFIGURAÇÕES DO WEBSOCKET -------------------------------------------
        // 
        // Endereço da rede local do ESP32
        var gateway = `ws://${window.location.hostname}/ws`;

        // Objeto Web Socket
        var websocket;

        // Event Listener para carregamento da página
        window.addEventListener('load', onLoad);

        // Inicialização das configurações do Web Socket
        function initWebSocket() {
            console.log('Trying to open a WebSocket connection...');
            websocket = new WebSocket(gateway);
            websocket.onopen    = onOpen;    // 1) Evento do Web Socket 
            websocket.onclose   = onClose;   // 2) Evento do Web Socket
            websocket.onmessage = onMessage; // 3) Evento do Web Socket
        }

        // 1) Função a ser executada após o carregamento do Web Socket
        function onOpen(event) {
            console.log('Connection opened');
        }

        // 2) Função a ser executada após o fechamento do Web Socket
        function onClose(event) {
            console.log('Connection closed');
            setTimeout(initWebSocket, 2000);
        }

        // 3) Função a ser executada após a recepção de dados do Web Socket
        function onMessage(event) {
            // A variável event.data contém o texto recebido pelo Web Socket
            // Tal texto foi enviado pelo servidor (ESP32) para o cliente (site)
            var state;
            state = event.data;

            document.getElementById('state').innerHTML = state;
            gameManager(false);
        }

        // Função executada com o carregamento da página
        function onLoad(event) {
            initWebSocket();
        }
        
        // Função que manda uma mensagem ao servidor (ESP32)
        function gameOver(){
            websocket.send('gameover');
        }
    </script>
</html>
)rawliteral";










// String contendo o site do ranking ----------------------------------------------------------------------
const char menu_html[] PROGMEM = R"rawliteral(

<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="styles.css">
        <title>Menu</title>

        <style>
            :root{
                --container-width: 90vw;
                --container-height: 90vh;
            }

            /* Deixa todos os elementos com dimensionamento correto */
            *{
                box-sizing: border-box;
            }

            /* Retira a margem do documento inteiro */
            body{
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;

                margin: 0px;  
                font-family: Arial, Helvetica, sans-serif; 
                height: 100vh;
                background-color: rgb(235, 235, 235);
                /* background-color: #071d03; */
                /* background-color: #f3ed96; */
            }

            .login-button, .login-button:hover, .login-button:visited, .login-button:focus{
                text-decoration: none;
                color: white;
            }

            .container {
                margin: 5% 3%;

                @media (min-width: $bp-bart) {
                    margin: 2%; 
                }

                @media (min-width: $bp-homer) {
                    margin: 2em auto;
                    max-width: $bp-homer;
                }
            }

            header {
                color: rgb(0, 0, 0);
                display: flex;
                justify-content:center;
                flex-direction: column;                         
                font-family: sans-serif;          
            }

            footer{
                justify-content: end;
            }

            .alinha{
                display:flex;
                flex-direction: row;
                justify-content: space-evenly;
            }
        </style>
    </head>
    
    <body>
        <header>
            <h2>Bem vindo ao Coyote!</h2>
        </header>
        
        <h2>Aperte em "Jogar" para iniciar a partida</h2> 
        <button id="play-button">JOGAR</button>
        <footer>
            <p> Boa sorte!</p>
        </footer>
    </body>
    <script>

        // Configuração do LocalStorage para a pontuação
        function setStorage(){
            if (typeof(Storage) !== "undefined"){
                // Caso não exista a variável no LocalStorage
                if (!localStorage.score){
                    let scoreArray = [0,[],false];
                    // Esse array consiste em:
                    // scoreArray = [
                    //          current_score, 
                    //          all_scores,
                    //          submitted
                    //        ]

                    let all_scores = [];

                    // Inicialização dos scores globais
                    for (let i=0; i<15; i++){
                        all_scores.push([0,"AAA"]);
                    }

                    scoreArray[1] = all_scores;

                    localStorage.score = JSON.stringify(scoreArray);
                }
                // Configuração inicial do score
                // if (localStorage.score){
                //     let scoreArray = JSON.parse(localStorage.score);

                //     // Inicialização dos scores globais
                //     for (let i=0; i<15; i++){
                //         scoreArray[1][i] = ([0,"AAA"]);
                //     }

                //     localStorage.score = JSON.stringify(scoreArray);
                // }
            
            } else {
                alert("LocalStorage is not working.");
            }
        }

        setStorage();

        var playButton = document.getElementById("play-button");
        playButton.addEventListener("click",()=>{
            playGame();
        });



        // CONFIGURAÇÕES DO WEBSOCKET -------------------------------------------
        // 
        // Endereço da rede local do ESP32
        var gateway = `ws://${window.location.hostname}/ws`;

        // Objeto Web Socket
        var websocket;

        // Event Listener para carregamento da página
        window.addEventListener('load', onLoad);

        // Inicialização das configurações do Web Socket
        function initWebSocket() {
            console.log('Trying to open a WebSocket connection...');
            websocket = new WebSocket(gateway);
            websocket.onopen    = onOpen;    // 1) Evento do Web Socket 
            websocket.onclose   = onClose;   // 2) Evento do Web Socket
            websocket.onmessage = onMessage; // 3) Evento do Web Socket
        }

        // 1) Função a ser executada após o carregamento do Web Socket
        function onOpen(event) {
            console.log('Connection opened');
        }

        // 2) Função a ser executada após o fechamento do Web Socket
        function onClose(event) {
            console.log('Connection closed');
            setTimeout(initWebSocket, 2000);
        }

        // 3) Função a ser executada após a recepção de dados do Web Socket
        function onMessage(event) {
            // A variável event.data contém o texto recebido pelo Web Socket
            // Tal texto foi enviado pelo servidor (ESP32) para o cliente (site)
            console.clear();
            console.log("Dados estão sendo recebidos.");
        }

        // Função executada com o carregamento da página
        function onLoad(event) {
            initWebSocket();
        }
        
        // Função que manda uma mensagem ao servidor (ESP32)
        function playGame(){
            websocket.send('play');
            document.location.href = '/main';
        }

    </script>
</html>
)rawliteral";





// String contendo o site do ranking ----------------------------------------------------------------------
const char ranking_html[] PROGMEM = R"rawliteral(

<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="styles.css">
        <title>Ranking</title>

        <style>
            :root{
                --container-width: 90vw;
                --container-height: 90vh;
            }

            /* Deixa todos os elementos com dimensionamento correto */
            *{
                box-sizing: border-box;
            }

            /* Retira a margem do documento inteiro */
            body{
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;

                margin: 0px;  
                font-family: Arial, Helvetica, sans-serif; 
                height: 100vh;
                background-color: rgb(235, 235, 235);
                /* background-color: #071d03; */
                /* background-color: #f3ed96; */
            }

            .login-button, .login-button:hover, .login-button:visited, .login-button:focus{
                text-decoration: none;
                color: white;
            }

            .container {
                margin: 5% 3%;

                @media (min-width: $bp-bart) {
                    margin: 2%; 
                }

                @media (min-width: $bp-homer) {
                    margin: 2em auto;
                    max-width: $bp-homer;
                }
            }

            header {
                color: rgb(0, 0, 0);
                display: flex;
                justify-content:center;
                flex-direction: column;                         
                font-family: sans-serif;          
            }

            footer{
                justify-content: end;
            }

            .alinha{
                display:flex;
                flex-direction: row;
                justify-content: space-evenly;
            }
        </style>
    </head>
    
    <body>
        <header>
        <h1>Fim de Jogo!</h1>
        
        </header>
        <h2>Sua pontuação nesta rodada: <span id="partida-score">x</span></h2>
        <div class="container">
            <table class="responsive-table">
            <caption>Top 15 Pontuações</caption>
            <thead>
                <tr>
                <th scope="col">Colocação</th>
                <th scope="col">Pontuação</th>
                <th scope="col">Nome</th>
                
                </tr>
            </thead>
            
            <tbody>
                <tr>
                    <th scope="row">1º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>           
                </tr>
                <tr>
                    <th scope="row">2º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td> 
                <tr>
                    <th scope="row">3º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">4º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">5º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">6º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">7º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">8º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">9º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                
                </tr>
                <tr>
                    <th scope="row">10º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">11º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">12º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">13º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">14º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
                <tr>
                    <th scope="row">15º</th>
                    <td data-title="Released" class="score-value"> 0</td>
                    <td data-title="Studio" class="score-name">AAA</td>
                </tr>
            </tbody>
            </table>
        </div>

        <div class = "alinha">
            <div>                        
                <input class="menu-input" type="text" name="query" id="id_query" placeholder="Digite suas iniciais..." />                           
                <button class="search-button" type="submit" id="submit_button" onclick="submitScore()">Submeter</button>
            </div>  
            <div class = "login-button">
            
                <button id="finish_button" onclick="document.location.href = '/';">Terminar</button>      
            </div>
        </div>
        
        <footer>
            <p>Obrigado por jogar!</p>
        </footer>
    </body>
    <script>

        // Configuração do LocalStorage
        function setStorage(){
            if (typeof(Storage) !== "undefined"){
                // Caso não exista a variável no LocalStorage
                if (!localStorage.score){
                    let scoreArray = [0,[],false];
                    // Esse array consiste em:
                    // scoreArray = [
                    //          current_score, 
                    //          all_scores,
                    //          submitted
                    //        ]

                    let all_scores = [];

                    // Inicialização dos scores globais
                    for (let i=0; i<15; i++){
                        all_scores.push([0,"AAA"]);
                    }

                    scoreArray[1] = all_scores;

                    localStorage.score = JSON.stringify(scoreArray);
                }
                // Configuração inicial do score
                if (localStorage.score){
                let scoreArray = JSON.parse(localStorage.score);
                let current_score = scoreArray[0];
                partida_score_tag.textContent = current_score;
                }
            
            } else {
                alert("LocalStorage is not working.");
            }
        }


        // Função que faz a ordenação das pontuações
        function sortScore(){

            if (localStorage.score){
                // Objeto de score armazenado no Local Storage
                let scoreArray = JSON.parse(localStorage.score);

                // Lista com todas as pontuações
                let all_scores = scoreArray[1];

                // Lista nova com as pontuações ordenadas
                let sorted_scores = [];
                for (let j=0; j<15; j++){
                    sorted_scores.push([0,"AAA"]);
                }

                // Ordenação das pontuações
                for (let i=0; i<15; i++){
                    if (all_scores[i][0] != 0){
                        let pos = -1;
                        for (let j=0; j<15; j++){
                            if (all_scores[i][0] <= all_scores[j][0]){
                                pos++;
                            }
                        }
                        while(sorted_scores[pos][0] != 0){
                            pos--;
                        }
                        sorted_scores[pos] = all_scores[i];
                        score_value_tag_array[pos].textContent = all_scores[i][0];
                        score_name_tag_array[pos].textContent = all_scores[i][1];
                    }  
                }

                if (scoreArray[2] == true){
                    name_input.style.display = "none";
                    submit_button.style.display = "none";
                    finish_button.style.display = "inline-block";
                }else{
                    name_input.style.display = "inline-block";
                    submit_button.style.display = "inline-block";
                    finish_button.style.display = "none";
                }
                scoreArray[1] = sorted_scores;
                localStorage.score = JSON.stringify(scoreArray);

            }else{
                alert("LocalStorage is not working.");
            }
            
        }


        // Submeter pontuação e nome
        function submitScore(){
            if (localStorage.score){
                if (name_input.value.length == 3){
                    // Objeto de score armazenado no Local Storage
                    let scoreArray = JSON.parse(localStorage.score);

                    // Lista com todas as pontuações
                    let current_score = scoreArray[0];

                    // Lista com todas as pontuações
                    let all_scores = scoreArray[1];

                    // Iniciais do jogador
                    let player_name = name_input.value;

                    // Variável com a menor pontuação do jogo
                    let min_score = all_scores[0][0];
                    let min_score_id = 0;
                    for (let i=0; i<15; i++){
                        if (all_scores[i][0] <= min_score){
                            min_score = all_scores[i][0];
                            min_score_id = i;
                        }
                    }
                    if (current_score >= min_score){
                        all_scores[min_score_id] = [current_score,player_name];
                    }


                    scoreArray[1] = all_scores;
                    scoreArray[2] = true;

                    localStorage.score = JSON.stringify(scoreArray);
                    finishGame();
                    document.location.href = "/ranking";

                }else{
                    alert("Insira apenas 3 letras");
                }
                
            }
        }

        // VARIÁVEIS GLOBAIS COM AS TAGS DE PONTUAÇÃO
        var score_value_tag_array = document.getElementsByClassName("score-value");
        var score_name_tag_array = document.getElementsByClassName("score-name");
        var partida_score_tag = document.getElementById("partida-score");

        // VARIÁVEIS GLOBAIS COM OS INPUTS DE SUBMISSÃO
        var name_input = document.getElementById("id_query");
        var submit_button = document.getElementById("submit_button");
        var finish_button = document.getElementById("finish_button");

        setStorage();
        sortScore();


         // CONFIGURAÇÕES DO WEBSOCKET -------------------------------------------
        // 
        // Endereço da rede local do ESP32
        var gateway = `ws://${window.location.hostname}/ws`;

        // Objeto Web Socket
        var websocket;

        // Event Listener para carregamento da página
        window.addEventListener('load', onLoad);

        // Inicialização das configurações do Web Socket
        function initWebSocket() {
            console.log('Trying to open a WebSocket connection...');
            websocket = new WebSocket(gateway);
            websocket.onopen    = onOpen;    // 1) Evento do Web Socket 
            websocket.onclose   = onClose;   // 2) Evento do Web Socket
            websocket.onmessage = onMessage; // 3) Evento do Web Socket
        }

        // 1) Função a ser executada após o carregamento do Web Socket
        function onOpen(event) {
            console.log('Connection opened');
        }

        // 2) Função a ser executada após o fechamento do Web Socket
        function onClose(event) {
            console.log('Connection closed');
            setTimeout(initWebSocket, 2000);
        }

        // 3) Função a ser executada após a recepção de dados do Web Socket
        function onMessage(event) {
            // A variável event.data contém o texto recebido pelo Web Socket
            // Tal texto foi enviado pelo servidor (ESP32) para o cliente (site)
            console.clear();
            console.log("Dados estão sendo recebidos.");
        }

        // Função executada com o carregamento da página
        function onLoad(event) {
            initWebSocket();
        }
        
        // Função que manda uma mensagem ao servidor (ESP32)
        function finishGame(){
            websocket.send('finaliza');
        }

    </script>
</html>
)rawliteral";







// Função que envia uma mensagem de volta ao cliente
// Tal mensagem será armazenada na variável STATE no HTML
void enviarDados(String mensagem) {
    ws.textAll(mensagem); 
}

void notifyClients() {
    ws.textAll(String(ledState)); 
}

// Função que recebe uma mensagem do site e faz algo com ela no ESP32
void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
    AwsFrameInfo *info = (AwsFrameInfo*)arg;

    if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
        data[len] = 0;
        if (strcmp((char*)data, "play") == 0) {
            current_event = "play";
        }
        if (strcmp((char*)data, "gameover") == 0) {
            current_event = "gameover";
        }
        if (strcmp((char*)data, "finaliza") == 0) {
            current_event = "finaliza";
        }
    }
}

// Eventos que podem acontecer com o Web Socket
void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type,
             void *arg, uint8_t *data, size_t len) {
    switch (type) {
        case WS_EVT_CONNECT:
            Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
            break;
        case WS_EVT_DISCONNECT:
            Serial.printf("WebSocket client #%u disconnected\n", client->id());
            break;
        case WS_EVT_DATA:
            handleWebSocketMessage(arg, data, len);
            break;
        case WS_EVT_PONG:
        case WS_EVT_ERROR:
            break;
    }
}

// Função que inicializa as configurações do Web Socket
void initWebSocket() {
    ws.onEvent(onEvent);
    server.addHandler(&ws);
}

// Configurações da String que armazena o site
String processor(const String& var){
    Serial.println(var);
    
    return String();
}

// Função de inicialização  (vai no setup)
void wifiSetup(){

    // Inicialização do pino do LED de teste
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, LOW);

    // Conexão ao WiFi
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.println("Connecting to WiFi..");
    }

    // Printa o endereço IP Local do ESP
    Serial.println(WiFi.localIP());

    // Inicializa o Web Socket
    initWebSocket();

    // Route para root / web page
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send_P(200, "text/html", menu_html, processor);
    });

    // Route para root / web page
    server.on("/main", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send_P(200, "text/html", index_html, processor);
    });

    // Route para página de ranking
    server.on("/ranking", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send_P(200, "text/html", ranking_html, processor);
    });

    // Inicializa servidor
    server.begin();
}

// Função do Loop  (vai no loop)
void wifiLoop() {
    ws.cleanupClients();
}