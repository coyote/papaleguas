#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

// #include <Servo.h> //ja foram incluidas
// #include <WiFi.h>
#include "potenciometro.h"
#include "servomotor.h"
#include "ultrassonico.h"
#include "testepartida.h"


//#define LED2 2       /*LED ligado ao GPIO14*/
//#define BT 0       /*Botão ligado ao pino GPIO12*/
#define NUM_ESTADOS 4 //numero de estados da maquina de estados
#define NUM_EVENTOS 6 //numero de eventos da maquina de estados

int ligar = 0; //flag para ligar e desligar ultrassom, potenciometro e servomotor
float distancia;
float angle;


/* Variáveis para Armazenar os handles das Tasks */
//TaskHandle_t xBot;                  /*handle da Task Botão*/
//TimerHandle_t xTimer2;              /*handle dos timers*/
TaskHandle_t SensorHandle;   //handle do sensor
TaskHandle_t ObterHandle;   //handle do sensor
TaskHandle_t MaquinaHandle;   //handle do sensor
TaskHandle_t ServoHandle;    //handle do servo
//TaskHandle_t SiteHandle;    //handle do servo
QueueHandle_t xQueue;


/* Protótipo das Tasks*/
void vSensor(void *pvParameters ); 
void vServo(void *pvParameters );
//void vBot( void * pvParameters );
void vObterEvento(void *pvParameters );
void vMaquinaEstados( void * pvParameters );
//void vSite( void * pvParameters );


/* Funções auxiliares */
void InitGeral(void); //inicia setup e funcoes
//void callBackTimer2(TimerHandle_t pxTimer ); //quando o timer2 acaba, ativa isso



enum estados {MENU,CONFIG,PARTIDA,RANKING};
enum eventos { WIFI_CONFIG,WIFI_OK,PLAY,ACERTO, GAMEOVER, FINALIZA, NENHUM_EVENTO};
enum acoes {A01, A02, A03, A04, A05,A06,  NENHUMA_ACAO};

// int obterEvento();
int obterAcao(int estado, int codigoEvento);
int obterProximoEstado(int estado, int codigoEvento);
void executarAcao(int codigoAcao);


void setup() {
  
    InitGeral();      /* Configuração de inicializacao */

    xQueue = xQueueCreate(5, sizeof(int));
    if (xQueue == NULL){
        Serial.println("Erro na criacao da fila");
    }

    /*Cria o timer2 com 1000ms de periodo com one shot*/
    // xTimer2 = xTimerCreate(
    //     "Timer2"
    //     , pdMS_TO_TICKS(15000)               //tempo do timer
    //     , pdFALSE
    //     , 0
    //     , callBackTimer2
    //     );

    xTaskCreate(
            vSensor                            /* função da task*/   
        ,  "Sensorando"                        /* Nome da Task */
        ,  configMINIMAL_STACK_SIZE + 500      /* Stack Size */
        ,  NULL                                /* parametro passado para a task*/
        ,  0                                   /* Prioridade da task*/
        ,  &SensorHandle                       /* handle da task*/
        );   

     xTaskCreate(
            vObterEvento                            /* função da task*/   
        ,  "Eventando"                        /* Nome da Task */
        ,  configMINIMAL_STACK_SIZE + 5000      /* Stack Size */
        ,  NULL                                /* parametro passado para a task*/
        ,  2                                   /* Prioridade da task*/
        ,  &ObterHandle                       /* handle da task*/
        );   
    
    xTaskCreate(
            vMaquinaEstados                            /* função da task*/   
        ,  "Maquinando"                        /* Nome da Task */
        ,  configMINIMAL_STACK_SIZE + 5000     /* Stack Size */
        ,  NULL                                /* parametro passado para a task*/
        ,  2                                   /* Prioridade da task*/
        ,  &MaquinaHandle                       /* handle da task*/
        );   


    xTaskCreate(
            vServo                          /* função da task*/
        ,  "Rodando"                        /* Nome da Task */
        ,  configMINIMAL_STACK_SIZE + 200   /* Stack Size,*/
        ,  NULL                             /* parametro passado para a task*/
        ,  1                                /* Prioridade da task*/
        ,  &ServoHandle                     /* handle da task*/
        );       

    // xTaskCreate(
    //     vBot
    //     ,  "Bot"
    //     ,  configMINIMAL_STACK_SIZE + 1024
    //     ,  NULL
    //     ,  3
    //     ,  &xBot
    //     );   

//    xTaskCreatePinnedToCore(
//        vSite
//        ,  "Site"
//        ,  configMINIMAL_STACK_SIZE + 1024
//        ,  NULL
//        ,  1
//        ,  &SiteHandle
//        ,  PRO_CPU_NUM
//        );  

     vTaskSuspend(SensorHandle); //inicia com sensor e servos suspensos
     vTaskSuspend(ServoHandle);
}

void InitGeral(void)
{
    ultrassonico_init();  //inicializa ultrassom
    servomotor_init();
    
    Serial.begin(115200);   /*inicializa a comunicação serial*/
    wifiSetup();
}


void loop() {} //Rtos não precisa


///* Task botão */
//void vBot( void * pvParameters )
//{
//    (void) pvParameters; //isso evita erros do compilador
//
//    uint8_t debouncingTime = 0;
//
//    while(1)
//    {
//    if(digitalRead(BT)==LOW){  //Botão pressionado
//        debouncingTime++;      
//        if(debouncingTime >= 10 )  //se ficar pressionado por 10 ticks, considera q foi pressionado mesmo
//        {
//            digitalWrite(LED2,HIGH);
//            ligar = 1;            //liga potenciometro, servomotor e ultrassom
//
//            debouncingTime = 0;
//            Serial.println("Timer 2 Start");
//            xTimerStart(xTimer2, 0);    //Timer2 liga e depois de certo tempo, chama o callback()
//        }
//    }
//    else{
//        debouncingTime = 0;
//    }
//
//    vTaskDelay(pdMS_TO_TICKS(10));
//    }
//}


//task sensores
void vSensor(void *pvParameters )
{
    
    (void) pvParameters; //evita erro de compilacao

    while(1)
    {   
      
        distancia = ultraSonic(1); //liga ultrassom
        angle = potencio(1); //liga potenciometro
        String medida = String(angle) + "," + String(distancia);
//        strcat(medida, String(angle));
//        strcat(medida, ",");
//        strcat(medida, String(distancia));
        enviarDados(medida);
//        Serial.println(ligar);
//        if (1){
//        Serial.println(angle);
//        Serial.println(distancia);
//        }
        delay(201);     //para liberar a tarefa
    }
}

//task servo
void vServo(void *pvParameters )
{
    // UBaseType_t uxHighWaterMark;
    (void) pvParameters;
    int posicao = 0;
    int posicao1 = 0;

    while(1)
    {   

      if (posicao1 == 0){
        servoTeste(1, posicao); //liga servo
        posicao += 20;
        if (posicao >= 180){
            posicao1 = 180;
        }
      }
      else{
        servoTeste(1, posicao); //liga servo
        posicao -= 20;
        if (posicao <= 0){
            posicao1 = 0;
        }
      
        
      }
        vTaskDelay(pdMS_TO_TICKS(100));
        
        // uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
        // Serial.print("Task1 Stack Size: ");
        // Serial.println(uxHighWaterMark);
        // Serial.println(" ");
    }
}


////task timer, é o callback
//void callBackTimer2(TimerHandle_t pxTimer )
//{
//    digitalWrite(LED2,LOW);//apaga LED 
//    ligar = 0;
//}


struct transicoes{
    int proximo_estado[NUM_ESTADOS][NUM_EVENTOS];
    int acoes[NUM_ESTADOS][NUM_EVENTOS];
};

struct transicoes Matriz = {
    {
        {CONFIG, MENU, PARTIDA,MENU,MENU,MENU},
        {CONFIG, MENU, CONFIG, CONFIG, CONFIG, CONFIG},
        {PARTIDA, PARTIDA, PARTIDA, PARTIDA,RANKING, PARTIDA},
        {RANKING, RANKING, RANKING, RANKING, RANKING, MENU}
        
    },
    {
        {A01, NENHUMA_ACAO, A03,NENHUMA_ACAO,NENHUMA_ACAO,NENHUMA_ACAO},
        {NENHUMA_ACAO, A02, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO},
        {NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, A04, A05, NENHUMA_ACAO},
        {NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, NENHUMA_ACAO, A06}
        
    }
};


void vMaquinaEstados(void *pvParameters) {
    (void) pvParameters;
    
    int codigoEvento;
    int codigoAcao;
    int estado;

    estado = MENU;

    printf ("Maquina de estados iniciada\n");
    for( ; ; ) {

        if( xQueueReceive( xQueue, &codigoEvento, portMAX_DELAY ) == pdPASS ) {

            if (codigoEvento != NENHUM_EVENTO)
            {
                codigoAcao = obterAcao(estado, codigoEvento);
                estado = obterProximoEstado(estado, codigoEvento);
                executarAcao(codigoAcao);
//                printf("Estado: %d Evento: %d Acao:%d\n", estado, codigoEvento, codigoAcao);
            }
        }
        delay(100);
    }
}

int obterAcao(int estado, int codigoEvento) {  
  return Matriz.acoes[estado][codigoEvento];
}

int obterProximoEstado(int estado, int codigoEvento) { 
  return Matriz.proximo_estado[estado][codigoEvento];
}

//char event_str[] = "wifi";

int read_count = 0;
char buf[20];

void vObterEvento(void *pvParameters) {   
    (void) pvParameters;
    int codigoEvento;
    BaseType_t xStatus;
    
//    char event_str[20];
//    printf("Entre com o evento: ");
//    scanf("%s", event_str);
//    delay(100);
    
    while(1){
        
        codigoEvento = NENHUM_EVENTO;
//        Serial.println(event_str);
//        if (Serial.available() > 0){
//            read_count = Serial.readBytesUntil('\n',buf, sizeof(buf)/sizeof(buf[0]) - 1);
//            Serial.println("hm");
//        }
        buf[read_count] = '\0';
        if(read_count > 0) {
//            Serial.println(buf);
        }
        if (strcmp(buf, "wifi_config") == 0) { 
//            char event_str[] = "wifi_confia";
//            Serial.println(event_str);
            codigoEvento = WIFI_CONFIG;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            if( xStatus != pdPASS ){
                Serial.println("Erro ao enviar evento para fila");
            }
            delay(100);
            continue;
        }
        else if (strcmp(buf, "wifi_ok") == 0) {
            codigoEvento = WIFI_OK;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            if( xStatus != pdPASS ){
                Serial.println("Erro ao enviar evento para fila");
            }
            delay(100);
            continue; 
        }
        else if ((strcmp(buf, "play") == 0) || current_event == "play") {
            codigoEvento = PLAY;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            if( xStatus != pdPASS ){
                Serial.println("Erro ao enviar evento para fila");
            }
            delay(100);
            continue;  
        }
        else if (strcmp(buf, "acerto") == 0) {
            codigoEvento = ACERTO;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            if( xStatus != pdPASS ){
                Serial.println("Erro ao enviar evento para fila");
            }
            delay(100);
            continue; 
        }
        else if ((strcmp(buf, "gameover") == 0) || current_event == "gameover") {
            codigoEvento = GAMEOVER;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            if( xStatus != pdPASS ){
                Serial.println("Erro ao enviar evento para fila");
            }
            delay(100);
            continue; 
        }
        else if ((strcmp(buf, "finaliza") == 0) || current_event == "finaliza") {
            codigoEvento = FINALIZA;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            if( xStatus != pdPASS ){
                Serial.println("Erro ao enviar evento para fila");
            }
            delay(100);
            continue; 
        }
//        buf[0] = 0;
        delay(1000);
    }
    
}


void executarAcao(int codigoAcao) { 
    if (codigoAcao == NENHUMA_ACAO)
        return;

    switch(codigoAcao)
    {
    case A01:
        printf("Wifi do ESP eh conectado no celular do jogador e ele acessa o site do jogo. Redirecionamento para a pagina de configuracoes (?).\n");
        break;
    case A02:
        printf("Leva o usuario de volta a pagina do menu e salva suas configuracoes.\n");
        break;
    case A03:
        printf("Leva o jogador a tela do jogo, na qual uma bolinha ja sera gerada em um lugar aleatorio. Tambem dispara o timer do jogo, inicia a pontuacao e o sensor ultrassonico.\n");
        vTaskResume(SensorHandle);
        vTaskResume(ServoHandle);
        break;
    case A04:
        printf("Atualiza a tela de jogo, computando o ponto obtido e gerando uma nova bola.\n");
        break;
    case A05:
        printf("Exibe mensagem de fim de jogo. Exibe também o ranking e disponibiliza a gravacao das iniciais do jogador.\n");
        vTaskSuspend(SensorHandle);
        vTaskSuspend(ServoHandle);
        break;
    case A06:
        printf("Retorna para a pagina do menu e salva alteracoes no ranking.\n");
        break;
   
    }
}