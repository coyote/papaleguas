#include <Arduino.h>
#include <stdio.h>


// Configurações do Potenciômetro
// 
// Potentiometer is connected to GPIO 34 (Analog ADC1_CH6) 
const int potPin = 34;

// variable for storing the potentiometer value
int potValue = 0;

// Ângulo de rotação do potenciômetro
float angulo = 0;   


// void potenciometro_init(){
//     printf("alo alo");
// }

float potencio(int ligar) {

    if (ligar){
        // Reading potentiometer value
        potValue = analogRead(potPin);
        angulo = (potValue*170)/4096;
        // Serial.println(angulo);
        // delay(100);
        return (angulo);
    }
    
}