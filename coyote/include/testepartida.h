#ifndef partida_included
#define partida_included

// Definição das funções do Web Server
void notifyClients(float mensagem);
void handleWebSocketMessage(void *arg, uint8_t *data, size_t len);
void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len);
void initWebSocket();
String processor(const String& var);
void wifiSetup();
void wifiLoop();
void enviarDados(String mensagem);
extern String current_event;


#endif